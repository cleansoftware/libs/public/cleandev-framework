from models import User
from models import _UserDataClass
from cleandev_framework import DataClassAdapter

if __name__ == '__main__':
    user: User = User(
        uuid='0548604f-4990-482b-977a-7c4164c816a9',
        username='Daniel',
        email='daniel@mail.com',
        lastname='Rodriguez'
    )

    user_data_class: _UserDataClass = DataClassAdapter.model_to_dataclass(user)
    user_data_class.__fields__  # ['uuid', 'username', 'email', 'lastname']
    user_data_class.__filter__(['username', 'email'])  # {'username': 'Daniel', 'email': 'daniel@mail.com'}
    # {'uuid': '0548604f-4990-482b-977a-7c4164c816a9', 'username': 'Daniel', 'email': 'daniel@mail.com', 'lastname': 'Rodriguez'}
