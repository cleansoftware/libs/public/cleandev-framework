from typing import Optional
from dataclasses import field
from sqlalchemy import String
from sqlalchemy import Column
from postgresql_db import Base
from dataclasses import dataclass
from cleandev_validator import DataClass
from postgresql_db.inmutables import _Params
from cleandev_validator import _DataClassConstrains


class User(Base):
    __tablename__ = 'user'

    uuid = Column(String, primary_key=True)
    username = Column(String)
    email = Column(String)
    lastname = Column(String)

    def __init__(self, **kwargs):
        self.uuid = kwargs.get(_Params.UUID)
        self.username = kwargs.get(_Params.USERNAME)
        self.email = kwargs.get(_Params.EMAIL)
        self.lastname = kwargs.get(_Params.LASTNAME)


@dataclass()
class _UserDataClass(DataClass):
    uuid: str
    username: str
    email: str
    lastname: Optional[str] = field(default=None)

    def __post_init__(self):
        super(_UserDataClass, self)._validate(**self.__dict__)

    @property
    def __constrains__(self):
        return {
            'uuid': str(_DataClassConstrains.STR),
            'username': str(_DataClassConstrains.STR),
            'email': str(_DataClassConstrains.STR),
            'lastname': str(_DataClassConstrains.STR)
        }
